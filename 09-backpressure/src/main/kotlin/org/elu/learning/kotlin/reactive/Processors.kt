package org.elu.learning.kotlin.reactive

import io.reactivex.Flowable
import io.reactivex.processors.PublishProcessor
import io.reactivex.rxkotlin.toFlowable
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import java.util.concurrent.TimeUnit

fun sampleProcessor() {
    val flowable = listOf("String 1", "String 2", "String 3", "String 4", "String 5").toFlowable()

    val processor = PublishProcessor.create<String>()

    processor.subscribe({
        println("Subscription 1: $it")
        runBlocking { delay(1000) }
        println("Subscription 1 delay")
    })
    processor.subscribe({ println("Subscription 2 $it") })

    flowable.subscribe(processor)
}

fun bufferSample() {
    val flowable = Flowable.range(1, 111)
    flowable.buffer(10)
            .subscribe { println(it) }
}

fun bufferWithSkip() {
    val flowable = Flowable.range(1, 111)
    flowable.buffer(10, 15)
            .subscribe { println("Subscription 1 $it") }

    flowable.buffer(15, 7)
            .subscribe { println("Subscription 2 $it") }
}

fun bufferWithInterval() {
    val flowable = Flowable.interval(100, TimeUnit.MILLISECONDS)
    flowable.buffer(1, TimeUnit.SECONDS)
            .subscribe { println(it) }

    runBlocking { delay(5, TimeUnit.SECONDS) }
}

fun bufferWithBoundary() {
    val boundaryFlowable = Flowable.interval(350, TimeUnit.MILLISECONDS)

    val flowable = Flowable.interval(100, TimeUnit.MILLISECONDS)
    flowable.buffer(boundaryFlowable)
            .subscribe { println(it) }

    runBlocking { delay(5, TimeUnit.SECONDS) }
}

fun windowSample() {
    val flowable = Flowable.range(1, 111)
    flowable.window(10)
            .subscribe { flo ->
                flo.subscribe {
                    print("$it, ")
                }
                println()
            }
}

fun throttleSample() {
    val flowable = Flowable.interval(100, TimeUnit.MILLISECONDS)
    flowable.throttleFirst(200, TimeUnit.MILLISECONDS)
            .subscribe { println(it) }

    runBlocking { delay(1, TimeUnit.SECONDS) }
}
