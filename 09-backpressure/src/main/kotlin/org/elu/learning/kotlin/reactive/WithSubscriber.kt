package org.elu.learning.kotlin.reactive

import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription

fun sampleSubscriber() {
    Flowable.range(1, 1000)
            .map { MyItem(it) }
            .observeOn(Schedulers.io())
            .subscribe(object : Subscriber<MyItem> {
                override fun onSubscribe(subscription: Subscription) {
                    subscription.request(Long.MAX_VALUE)
                }

                override fun onNext(s: MyItem?) {
                    runBlocking { delay(50) }
                    println("Subscriber received " + s!!)
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                }

                override fun onComplete() {
                    println("Done!")
                }
            })
    runBlocking { delay(60000) }
}

fun sampleSubscriberAndRequest() {
    Flowable.range(1, 15)
            .map { MyItem(it) }
            .observeOn(Schedulers.io())
            .subscribe(object : Subscriber<MyItem> {
                lateinit var subscription: Subscription
                override fun onSubscribe(subscription: Subscription) {
                    this.subscription = subscription
                    subscription.request(5)
                }

                override fun onNext(s: MyItem?) {
                    runBlocking { delay(50) }
                    println("Subscriber received " + s!!)
                    if (s.id == 5) {//(3)
                        println("Requesting two more")
                        subscription.request(2)
                    }
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                }

                override fun onComplete() {
                    println("Done!")
                }
            })
    runBlocking { delay(10000) }
}
