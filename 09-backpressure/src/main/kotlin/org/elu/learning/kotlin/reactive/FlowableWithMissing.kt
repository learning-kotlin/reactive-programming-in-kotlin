package org.elu.learning.kotlin.reactive

import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking

fun onBufferSample() {
    val source = Observable.range(1, 1000)
    source.toFlowable(BackpressureStrategy.MISSING)
            .onBackpressureBuffer()
            .map { MyItem(it) }
            .observeOn(Schedulers.io())
            .subscribe {
                println(it)
                runBlocking { delay(1000) }
            }
    runBlocking { delay(600000) }
}

fun onBufferSampleWithBufferLimit() {
    val source = Observable.range(1, 1000)
    source.toFlowable(BackpressureStrategy.MISSING)
            .onBackpressureBuffer(20)
            .map { MyItem(it) }
            .observeOn(Schedulers.io())
            .subscribe {
                println(it)
                runBlocking { delay(1000) }
            }
    runBlocking { delay(600000) }
}

fun onDropSample() {
    val source = Observable.range(1, 1000)
    source.toFlowable(BackpressureStrategy.MISSING)
            .onBackpressureDrop { print("Dropped $it;\t") }
            .map { MyItem3(it) }
            .observeOn(Schedulers.io())
            .subscribe {
                print("Rec. $it;\t")
                runBlocking { delay(1000) }
            }
    runBlocking { delay(600000) }
}

fun onLatestSample() {
    val source = Observable.range(1, 1000)
    source.toFlowable(BackpressureStrategy.MISSING)
            .onBackpressureLatest()
            .map { MyItem3(it) }
            .observeOn(Schedulers.io())
            .subscribe {
                print("-> $it;\t")
                runBlocking { delay(100) }
            }
    runBlocking { delay(600000) }
}

data class MyItemFlowable(val id: Int) {
    init {
        println("MyItemFlowable Created $id")
    }
}

object GenerateFlowableItem {
    var item: Int = 0
        get() {
            field += 1
            return field
        }
}

fun flowableGenerateSample() {
    val flowable = Flowable.generate<Int> {
        it.onNext(GenerateFlowableItem.item)
    }

    flowable
            .map { MyItemFlowable(it) }
            .observeOn(Schedulers.io())
            .subscribe {
                runBlocking { delay(100) }
                println("Next $it")
            }

    runBlocking { delay(700000) }
}
