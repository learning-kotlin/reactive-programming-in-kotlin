package org.elu.learning.kotlin.reactive

import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking

fun flowableFromObservable() {
    val source = Observable.range(1, 1000)
    source.toFlowable(BackpressureStrategy.BUFFER)
            .map { MyItem3(it) }
            .observeOn(Schedulers.io())
            .subscribe {
                print("Rec. $it;\t")
                runBlocking { delay(1000) }
            }
    runBlocking { delay(100000) }
}

fun flowableWithBackpressureError() {
    val source = Observable.range(1, 1000)
    source.toFlowable(BackpressureStrategy.ERROR)
            .map { MyItem(it) }
            .observeOn(Schedulers.io())
            .subscribe {
                println(it)
                runBlocking { delay(600) }
            }
    runBlocking { delay(700000) }
}

fun flowableWithBackpressureDrop() {
    val source = Observable.range(1, 1000)
    source.toFlowable(BackpressureStrategy.DROP)
            .map { MyItem(it) }
            .observeOn(Schedulers.computation())
            .subscribe {
                println(it)
                runBlocking { delay(1000) }
            }
    runBlocking { delay(700000) }
}
