package org.elu.learning.kotlin.reactive

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking

data class MyItem3(val id: Int) {
    init {
        print("MyItem Created $id;\t")
    }
}

fun sampleWithoutFlowable() {
    Observable.range(1, 1000)
            .map { MyItem3(it) }
            .observeOn(Schedulers.computation())
            .subscribe(
                    {
                        print("Received $it;\t")
                        runBlocking { delay(50) }
                    },
                    { it.printStackTrace() })
    runBlocking { delay(60000) }
}

fun sampleWithFlowable() {
    Flowable.range(1, 1000)
            .map { MyItem3(it) }
            .observeOn(Schedulers.io())
            .subscribe(
                    {
                        println("Received $it")
                        runBlocking { delay(50) }
                    },
                    { it.printStackTrace() })
    runBlocking { delay(60000) }
}
