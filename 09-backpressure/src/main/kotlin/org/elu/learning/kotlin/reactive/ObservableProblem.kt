package org.elu.learning.kotlin.reactive

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking

fun sampleProblem() {
    val observable = Observable.just(1, 2, 3, 4, 5, 6, 7, 8, 9)
    val subject = BehaviorSubject.create<Int>()
    subject.observeOn(Schedulers.computation())
            .subscribe({
                println("Subs 1 Received $it")
                runBlocking { delay(200) }
            })

    subject.observeOn(Schedulers.computation())
            .subscribe({
                println("Subs 2 Received $it")
            })
    observable.subscribe(subject)
    runBlocking { delay(2000) }
}

data class MyItem(val id: Int) {
    init {
        println("MyItem Created $id")
    }
}

fun anotherSampleProblem() {
    val observable = Observable.just(1, 2, 3, 4, 5, 6, 7, 8, 9)
    observable
            .map { MyItem(it) }
            .observeOn(Schedulers.computation())
            .subscribe({
                println("Received $it")
                runBlocking { delay(200) }
            })
    runBlocking { delay(2000) }
}
