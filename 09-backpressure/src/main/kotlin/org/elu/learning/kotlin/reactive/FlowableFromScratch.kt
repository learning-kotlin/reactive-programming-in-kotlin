package org.elu.learning.kotlin.reactive

import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription

val observer: Observer<Int> = object : Observer<Int> {
    override fun onComplete() {
        println("All Completed")
    }

    override fun onNext(item: Int) {
        println("Next $item")
    }

    override fun onError(e: Throwable) {
        println("Error Occured ${e.message}")
    }

    override fun onSubscribe(d: Disposable) {
        println("New Subscription ")
    }
}

fun flowableFromScratch() {
    val observable: Observable<Int> = Observable.create<Int> {
        for (i in 1..10) {
            it.onNext(i)
        }
        it.onComplete()
    }

    observable.subscribe(observer)
}

val subscriber: Subscriber<Int> = object : Subscriber<Int> {
    override fun onComplete() {
        println("All Completed")
    }

    override fun onNext(item: Int) {
        println("Next $item")
    }

    override fun onError(e: Throwable) {
        println("Error Occurred ${e.message}")
    }

    override fun onSubscribe(subscription: Subscription) {
        println("New Subscription ")
        subscription.request(10)
    }
}

fun sameWithFlowable() {
    val flowable: Flowable<Int> = Flowable.create<Int>({
        for (i in 1..10) {
            it.onNext(i)
        }
        it.onComplete()
    }, BackpressureStrategy.BUFFER)

    flowable
            .observeOn(Schedulers.io())
            .subscribe(subscriber)

    runBlocking { delay(10000) }
}
