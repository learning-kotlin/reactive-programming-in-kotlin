package org.elu.learning.kotlin.reactive

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async

fun main(args: Array<String>) {
    println("Initial Out put with a = 15, b = 10")
    val calculator = ReactiveCalculator(15, 10)

    println("Enter a = <number> or b = <number> in separate lines\nexit to exit the program")
    var line: String?
    do {
        line = readLine()
        async(CommonPool) {
            calculator.handleInput(line)
        }
    } while (line != null && !line.toLowerCase().contains("exit"))
}
