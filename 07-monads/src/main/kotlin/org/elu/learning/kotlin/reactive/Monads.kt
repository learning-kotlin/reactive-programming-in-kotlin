package org.elu.learning.kotlin.reactive

import io.reactivex.Maybe
import io.reactivex.rxkotlin.subscribeBy

fun sampleMonads() {
    val maybeValue: Maybe<Int> = Maybe.just(14)
    maybeValue.subscribeBy(
            onComplete = { println("Completed Empty") },
            onError = { println("Error $it") },
            onSuccess = { println("Completed with value $it") }
    )
    val maybeEmptyValue: Maybe<Int> = Maybe.empty()
    maybeEmptyValue.subscribeBy(
            onComplete = { println("Completed Empty") },
            onError = { println("Error $it") },
            onSuccess = { println("Completed with value $it") }
    )
}
