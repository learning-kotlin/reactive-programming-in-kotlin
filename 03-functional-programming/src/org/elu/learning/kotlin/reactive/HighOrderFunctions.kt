package org.elu.learning.kotlin.reactive

fun highOrderFunction(a: Int, validityCheckFunc: (a: Int) -> Boolean) {
    if (validityCheckFunc(a)) {
        println("a $a is valid")
    } else {
        println("a $a is invalid")
    }
}

fun main(args: Array<String>) {
    highOrderFunction(12, { a: Int -> a.isEven()})
    highOrderFunction(19, { a: Int -> a.isEven()})
}

private fun Int.isEven(): Boolean = this % 2 == 0
