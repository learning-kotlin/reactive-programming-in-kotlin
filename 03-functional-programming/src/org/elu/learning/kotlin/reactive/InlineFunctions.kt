package org.elu.learning.kotlin.reactive

inline fun doSomeStaff(a: Int = 0) = a + (a * a)

inline fun highOrderInlineFunctionIn(a: Int, validityCheckFunc: (a: Int) -> Boolean) {
    if (validityCheckFunc(a)) {
        println("a $a is valid")
    } else {
        println("a $a is invalid")
    }
}

private fun Int.isEven(): Boolean = this % 2 == 0

fun main(args: Array<String>) {
    for (i in 1..10) {
        println("$i output ${doSomeStaff(i)}")
    }

    highOrderInlineFunctionIn(12, { a: Int -> a.isEven()})
    highOrderInlineFunctionIn(19, { a: Int -> a.isEven()})
}
