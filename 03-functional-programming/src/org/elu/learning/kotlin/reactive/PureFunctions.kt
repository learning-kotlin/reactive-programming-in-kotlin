package org.elu.learning.kotlin.reactive

fun square(n: Int) = n * n

fun main(args: Array<String>) {
    println("named pure function square = ${square(3)}")

    val cube = { n: Int -> n * n * n }
    println("lambda pure function cube = ${cube(3)}")
}
