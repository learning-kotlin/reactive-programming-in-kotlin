package org.elu.learning.kotlin.reactive

import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.rxkotlin.toObservable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

fun main(args: Array<String>) {
    val list = listOf("One", 2, "Three", "Four", 4.5, "Five", 6.0f)

    // iterative
    val iterator = list.iterator()
    while (iterator.hasNext()) {
        println(iterator.next())
    }
    println("===== 1")
    println()

    // reactive
    val observable = list.toObservable()
    observable.subscribeBy(
            onNext = { println(it) },
            onError = { it.printStackTrace() },
            onComplete = { println("Done!") }
    )
    println("===== 2")
    println()

    // using Subject
    val subject: Subject<Int> = PublishSubject.create()

    subject.map({ isEven(it) }).subscribe({
        println("The number is ${(if (it) "Even" else "Odd")}")
    })

    subject.onNext(4)
    subject.onNext(9)
}

fun isEven(x: Int) = x % 2 == 0

