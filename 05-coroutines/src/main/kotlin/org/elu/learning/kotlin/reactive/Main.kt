package org.elu.learning.kotlin.reactive

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import java.util.concurrent.TimeUnit
import kotlin.coroutines.experimental.buildSequence
import kotlin.system.measureTimeMillis

suspend fun longRunningTask(): Long {
    val time = measureTimeMillis {
        println("Please wait")
        delay(2, TimeUnit.SECONDS)
        println("Delay is over")
    }
    return time
}

fun fibonacciSample() {
    var a = 0
    var b = 1
    print("$a, ")
    print("$b, ")

    for (i in 2..9) {
        val c = a + b
        print("$c, ")
        a = b
        b = c
    }
}

fun fibonacciWithSeries() {
    val fibonacciSeries = buildSequence {
        var a = 0
        var b = 1
        yield(a)
        yield(b)

        while (true) {
            val c = a + b
            yield(c)
            a = b
            b = c
        }
    }
    println(fibonacciSeries.take(10).joinToString(", "))
}

fun main(args: Array<String>) {
    runBlocking {
        val exeTime = longRunningTask()
        println("Execution Time is $exeTime")
    }

    val time = async(CommonPool) { longRunningTask() }
    println("print after async")
    runBlocking { println("printing time ${time.await()}") }

    fibonacciSample()
    println()

    fibonacciWithSeries()
    println()
}
