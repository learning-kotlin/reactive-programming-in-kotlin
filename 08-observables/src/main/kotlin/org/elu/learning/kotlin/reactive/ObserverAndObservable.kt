package org.elu.learning.kotlin.reactive

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.toObservable
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import java.util.concurrent.Callable
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

val observer: Observer<Any> = object : Observer<Any> {
    override fun onComplete() {
        println("All Completed")
    }

    override fun onNext(item: Any) {
        println("Next $item")
    }

    override fun onError(e: Throwable) {
        println("Error occurred $e")
    }

    override fun onSubscribe(d: Disposable) {
        println("New Subscription ")
        println("Subscribed to $d")
    }
}

fun sampleObserverObservable() {
    val observable: Observable<Any> = listOf("One", 2, "Three", "Four", 4.5, "Five", 6.0f).toObservable()

    observable.subscribe(observer)

    val observableOnList: Observable<List<Any>> = Observable.just(
            listOf("One", 2, "Three", "Four",
                    4.5, "Five", 6.0f),
            listOf("List with Single Item"),
            listOf(1, 2, 3, 4, 5, 6))
    observableOnList.subscribe(observer)
}

fun anotherSampleObservable() {
    val observable: Observable<String> = Observable.create<String> {
        it.onNext("Emit 1")
        it.onNext("Emit 2")
        it.onNext("Emit 3")
        it.onNext("Emit 4")
        it.onComplete()
    }
    observable.subscribe(observer)

    val observable2: Observable<String> = Observable.create<String> {
        it.onNext("Emit 1")
        it.onNext("Emit 2")
        it.onNext("Emit 3")
        it.onNext("Emit 4")
        it.onError(Exception("My Custom Exception"))
    }
    observable2.subscribe(observer)
}

fun complexSampleObservable() {
    val list = listOf("String 1", "String 2", "String 3", "String 4")
    val observableFromIterable: Observable<String> = Observable.fromIterable(list)//1
    observableFromIterable.subscribe(observer)

    val callable = Callable<String> { "From Callable" }
    val observableFromCallable: Observable<String> = Observable.fromCallable(callable)
    observableFromCallable.subscribe(observer)

    val future: Future<String> = object : Future<String> {
        override fun get(): String = "Hello From Future"

        override fun get(timeout: Long, unit: TimeUnit?): String = "Hello From Future"

        override fun isDone(): Boolean = true

        override fun isCancelled(): Boolean = false

        override fun cancel(mayInterruptIfRunning: Boolean):
                Boolean = false

    }
    val observableFromFuture: Observable<String> = Observable.fromFuture(future)//3
    observableFromFuture.subscribe(observer)
}

fun observableExtensionFunc() {
    val list: List<String> = listOf("String 1", "String 2", "String 3", "String 4")

    val observable: Observable<String> = list.toObservable()

    observable.subscribe(observer)
}

fun observableJustSample() {
    Observable.just("A String").subscribe(observer)
    Observable.just(54).subscribe(observer)
    Observable.just(listOf("String 1", "String 2", "String 3", "String 4")).subscribe(observer)
    Observable.just(mapOf(Pair("Key 1", "Value 1"), Pair("Key 2", "Value 2"), Pair("Key 3", "Value 3"))).subscribe(observer)
    Observable.just(arrayListOf(1, 2, 3, 4, 5, 6)).subscribe(observer)
    Observable.just("String 1", "String 2", "String 3").subscribe(observer)
}

fun otherObservableFactoryMethods() {
    Observable.range(1, 10).subscribe(observer)
    Observable.empty<String>().subscribe(observer)

    runBlocking {
        Observable.interval(300, TimeUnit.MILLISECONDS).subscribe(observer)
        delay(900)
        Observable.timer(400, TimeUnit.MILLISECONDS).subscribe(observer)
        delay(450)
    }
}

fun subscribing() {
    val observable: Observable<Int> = Observable.range(1, 5)

    observable.subscribe({
        //onNext method
        println("Next $it")
    }, {
        //onError Method
        println("Error ${it.message}")
    }, {
        //onComplete Method
        println("Done")
    })

    observable.subscribe(observer)
}

fun usingDisposable() {
    runBlocking {
        val observable: Observable<Long> = Observable.interval(100, TimeUnit.MILLISECONDS)
        val innerObserver: Observer<Long> = object : Observer<Long> {
            lateinit var disposable: Disposable

            override fun onSubscribe(d: Disposable) {
                disposable = d
            }

            override fun onNext(item: Long) {
                println("Received $item")
                if (item >= 10 && !disposable.isDisposed) {
                    disposable.dispose()
                    println("Disposed")
                }
            }

            override fun onError(e: Throwable) {
                println("Error ${e.message}")
            }

            override fun onComplete() {
                println("Complete")
            }
        }
        observable.subscribe(innerObserver)
        delay(1500)
    }
}
