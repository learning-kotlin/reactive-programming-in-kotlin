package org.elu.learning.kotlin.reactive

import io.reactivex.Observable
import io.reactivex.subjects.AsyncSubject
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.ReplaySubject

fun asyncSubjectSample() {
    val observable = Observable.just(1, 2, 3, 4)
    val subject = AsyncSubject.create<Int>()
    observable.subscribe(subject)
    subject.subscribe({
        //onNext
        println("Received $it")
    }, {
        //onError
        it.printStackTrace()
    }, {
        //onComplete
        println("Complete")
    })
    subject.onComplete()
}

fun asyncSubjectAnotherSample() {
    val subject = AsyncSubject.create<Int>()
    subject.onNext(1)
    subject.onNext(2)
    subject.onNext(3)
    subject.onNext(4)
    subject.subscribe({
        //onNext
        println("S1 Received $it")
    }, {
        //onError
        it.printStackTrace()
    }, {
        //onComplete
        println("S1 Complete")
    })
    subject.onNext(5)
    subject.subscribe({
        //onNext
        println("S2 Received $it")
    }, {
        //onError
        it.printStackTrace()
    }, {
        //onComplete
        println("S2 Complete")
    })
    subject.onComplete()
}

fun behaviorSubjectSample() {
    val subject = BehaviorSubject.create<Int>()
    subject.onNext(1)
    subject.onNext(2)
    subject.onNext(3)
    subject.onNext(4)
    subject.subscribe({
        //onNext
        println("S1 Received $it")
    }, {
        //onError
        it.printStackTrace()
    }, {
        //onComplete
        println("S1 Complete")
    })
    subject.onNext(5)
    subject.subscribe({
        //onNext
        println("S2 Received $it")
    }, {
        //onError
        it.printStackTrace()
    }, {
        //onComplete
        println("S2 Complete")
    })
    subject.onComplete()
}

fun replaySubjectSample() {
    val subject = ReplaySubject.create<Int>()
    subject.onNext(1)
    subject.onNext(2)
    subject.onNext(3)
    subject.onNext(4)
    subject.subscribe({
        //onNext
        println("S1 Received $it")
    }, {
        //onError
        it.printStackTrace()
    }, {
        //onComplete
        println("S1 Complete")
    })
    subject.onNext(5)
    subject.subscribe({
        //onNext
        println("S2 Received $it")
    }, {
        //onError
        it.printStackTrace()
    }, {
        //onComplete
        println("S2 Complete")
    })
    subject.onComplete()
}
