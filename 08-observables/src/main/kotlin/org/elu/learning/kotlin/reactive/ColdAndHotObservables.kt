package org.elu.learning.kotlin.reactive

import io.reactivex.Observable
import io.reactivex.rxkotlin.toObservable
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import java.util.concurrent.TimeUnit
import javax.xml.ws.Endpoint.publish

fun coldObservable() {
    val observable: Observable<String> = listOf("String 1", "String 2", "String 3", "String 4").toObservable()

    observable.subscribe({
        println("Received $it")
    }, {
        println("Error ${it.message}")
    }, {
        println("Done")
    })

    observable.subscribe({
        println("Received $it")
    }, {
        println("Error ${it.message}")
    }, {
        println("Done")
    })
}

fun sampleConnectableObservable() {
    val connectableObservable = listOf("String 1", "String 2", "String 3", "String 4", "String 5").toObservable()
            .publish()
    connectableObservable.subscribe({ println("Subscription 1: $it") })
    connectableObservable.map(String::reversed).subscribe({ println("Subscription 2 $it") })
    connectableObservable.connect()
    connectableObservable.subscribe({ println("Subscription 3: $it") }) // Will not receive emissions
}

fun anotherHotObservable() {
    val connectableObservable = Observable.interval(100, TimeUnit.MILLISECONDS)
            .publish()
    connectableObservable.subscribe({ println("Subscription 1: $it") })
    connectableObservable.subscribe({ println("Subscription 2 $it") })
    connectableObservable.connect()
    runBlocking { delay(500) }

    connectableObservable.subscribe({ println("Subscription 3: $it") })
    runBlocking { delay(500) }
}

fun sampleSubject() {
    val observable = Observable.interval(100, TimeUnit.MILLISECONDS)//1
    val subject = PublishSubject.create<Long>()
    observable.subscribe(subject)
    subject.subscribe({
        println("Received $it")
    })
    runBlocking { delay(1100) }
}

fun anotherSubjectSample() {
    val observable = Observable.interval(100, TimeUnit.MILLISECONDS)//1
    val subject = PublishSubject.create<Long>()
    observable.subscribe(subject)
    subject.subscribe({
        println("Subscription 1 Received $it")
    })
    runBlocking { delay(1100) }
    subject.subscribe({
        println("Subscription 2 Received $it")
    })
    runBlocking { delay(1100) }
}
